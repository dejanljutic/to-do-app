# To-do app

To-do app is a simple javascript application for tracking your to-do list. To-dos are created with their "completed" property unchecked, meaning they are not finished. By checking the check-box next to a to-do you mark it as completed. The button next to the to-do is used for removing a to-do. To-dos can be filtered with the text-box on top, looking for specific words, or they can be filtered using a check-box to show only the to-dos which are yet to be completed.

## UUID

A third party library for generating an unique ID property for every to-do. 
It is imported to index.html as a local .js file, because the link provided in the course is not working properly. It is meant to be a read-only library, thus the formating is non-existent.

## Launching

The app needs to be launched using live-server. 

To install live-server you first need to install [Node](https://nodejs.org/en/).

By installing Node, you also install another program called npm - Node Package Manager. To see if it is correctly installed, run:
```bash
npm -v
```

Now to install live-server you need to run the following command:
```bash 
npm install -g live-server
```

Finally, to host the application you need to cd into the folder in which the app folder is located, and run the following command (Notes-app being the folder name):
```bash
live-server Todo-app
```

## Future improvements

CSS will be added, and a free hosting service will be used, making it much easier to use the app.